from flask_restful import Resource, reqparse

from models.contact import ContactModel
from models.email import EmailModel

class Contact(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('first_name',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!")
    parser.add_argument('last_name',
                        type=str,
                        required=True,
                        help="This field cannot be left blank!")
    parser.add_argument('email',
                        type=str,
                        required=False,
                        help="This field can be left blank!")
    parser.add_argument('contact_id',
                        type=int,
                        required=False,
                        help="This field can be left blank!")


    def get(self, username):
        contact = ContactModel.find_by_username(username)
        if contact:
            return contact.json()
        return {'message': 'Contact not found'}, 404

    def post(self, username):
        if ContactModel.find_by_username(username):
            return {'message': f"A contact with username '{username}' already exists."}, 400
        data = Contact.parser.parse_args()
        contact = ContactModel(username, data.get('first_name'), data.get('last_name'))
        if data.get('email'):
            email = EmailModel(data.get('email'), data.get('contact_id'))
        try:
            if email:
                email.add_to_db()
            contact.add_to_db()
        except:
            return {'message': 'An error occurred inserting the contact.'}, 500
        return contact.json(), 201

    def delete(self, username):
        contact = ContactModel.find_by_username(username)
        if contact:
            contact.delete_from_db()
        return {'message': 'Contact deleted'}

    def put(self, username):
        data = Contact.parser.parse_args()
        contact = ContactModel.find_by_username(username)
        if data.get('email'):
            email = EmailModel(data.get('email'), data.get('contact_id'))
        if not contact:
            if email:
                email.add_to_db()
            contact = ContactModel(username, data.get('first_name'), data.get('last_name'))
        else:
            if email:
                email.add_to_db()
            contact.first_name = data.get('first_name')
            contact.last_name = data.get('last_name')

        contact.add_to_db()
        return contact.json()

class Contacts(Resource):

    def get(self):
        return {'contacts': [contact.json() for contact in ContactModel.query.all()]}
