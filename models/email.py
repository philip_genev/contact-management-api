from db import db


class EmailModel(db.Model):
    __tablename__ = 'emails'

    email_id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(20))
    contact_id = db.Column(db.Integer, db.ForeignKey('contacts.contact_id'))
    contact = db.relationship('ContactModel')

    def __init__(self, email, contact_id):
        self.email = email
        self.contact_id = contact_id

    def add_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
