from db import db


class ContactModel(db.Model):
    __tablename__ = 'contacts'

    contact_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20))
    first_name = db.Column(db.String(20))
    last_name = db.Column(db.String(20))
    emails = db.relationship('EmailModel', lazy='dynamic')

    def __init__(self, username, first_name, last_name):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name

    def json(self):
        json_data = { 'username': self.username,
                      'first_name': self.first_name,
                      'last_name': self.last_name }
        if self.emails:
            json_data['emails'] = [em.email for em in self.emails.all()]
        return json_data

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    def add_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()
