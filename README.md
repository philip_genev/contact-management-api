## Setup the environment

1. Clone the repository in a suitable location and get into the `contact-management-api` folder
2. Create a new virtual environment as follows: `virtualenv --python=/usr/bin/python3.7 contact-management-venv`
3. Activate the new environment `source contact-management-env/bin/activate`
4. Install the required libraries `pip3 install -r requirements.txt`

 Note: The application has been written in `Python 3.7.3`
---

## Run the app

```$ python app.py```

---
## Test the app

1. Install Postman ---> https://www.getpostman.com
2. Load the json file in Postman ---> `Contact Management API.postman_collection.json`
3. Play with the endpoints in order to verify that the Contact API is working

  Note: Point 3 from the application was not implemented due to problems between Redis, Celery and Python3.7. 
